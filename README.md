# Queue Stats

Tool to monitor queues.

**Disclaimer** This tool consumes queue messages

## Requirements

### Production

- python 2.7.x [(link to downloads)](https://www.python.org/downloads/)

- pip

```
    curl -O https://bootstrap.pypa.io/get-pip.py && python get-pip.py
```

### Development

- [virtualenv](#virtual-environment) (optional)

    `pip install virtualenv`

- Install project dependencies

    `pip install -e .`

## Installation

Inside project folder or unpacked tarball, run:

```
    python setup.py install
```

## Usage

To use it:

    $ qstats --help

--------------------------------------

## Dist

To build dist, inside project folder, run:

    python setup.py sdist

## Virtual environment

[Virtualenv](https://virtualenv.pypa.io/en/stable/) is a tool to create isolated Python environments.

To create a new virtualenv run:

    virtualenv venv

To activate virtualenv, run activate script:

    source venv/bin/activate

To deactivate virtualenv, just run:

    deactivate
