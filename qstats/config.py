RMQ_HOST = 'localhost'
RMQ_PORT = 5672
RMQ_USER = 'target'
RMQ_PASSWORD = 'qwe123'
RMQ_VHOST = '/'
RMQ_QUEUES = [
    'hello',
    'http',
    'oracle',
    'mysql',
    'db2',
    'sqlserver',
    'postgres',
    'tls',
    'dns',
    'flow_app_vlan',
    'tcp',
    'scsi',
    'ajp',
]
