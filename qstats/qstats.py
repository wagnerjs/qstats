
import config
import curses
import pika
import signal
import sys
import threading
import time


class StoppableThread(threading.Thread):
    """Thread class with a stop() method.
    The thread itself has to check regularly for the stopped() condition."""

    def __init__(self):
        super(StoppableThread, self).__init__()
        self._stop = False

    def stop(self):
        self._stop = True

    def stopped(self):
        return self._stop


class QueueThread(StoppableThread):
    def __init__(self):
        super(QueueThread, self).__init__()

        self.queues_info = []
        self.error= None

    def run(self):
        try:
            credentials = pika.PlainCredentials(
                config.RMQ_USER,
                config.RMQ_PASSWORD
            )
            conn_params = pika.ConnectionParameters(
                config.RMQ_HOST,
                config.RMQ_PORT,
                config.RMQ_VHOST,
                credentials
            )
            self.connection = pika.BlockingConnection(conn_params)
            self.channel = self.connection.channel()

        except Exception as e:
            self.stop()

            self.error = dict(
                msg='Cant connect with RabbitMQ: {}:{}'.format(
                    config.RMQ_HOST,
                    config.RMQ_PORT,
                ),
                bt=e,
            )

        try:
            self.declare_queues()
            self.consume_queues()
        except Exception as e:
            self.stop()

            self.error = dict(
                msg='Problem on consume',
                bt=e,
            )

    def declare_queues(self):
        for queue_name in config.RMQ_QUEUES:
            try:
                self.channel.queue_declare(queue=queue_name)
            except Exception as e:
                self.channel = self.connection.channel()

    def consume_queues(self):
        for queue_name in config.RMQ_QUEUES:
            self.channel.basic_consume(
                self.handle_msg,
                queue=queue_name,
                no_ack=True,
            )

        while self.channel._consumer_infos and self.stopped() == False:
            self.channel.connection.process_data_events(time_limit=1)

        self.connection.close()

    def handle_msg(self, ch, method, properties, body):
        self.update_info(method.routing_key, body)

    def get_info(self):
        return self.queues_info

    def update_info(self, queue_id, data):
        queue_info = None

        for q_info in self.queues_info:
            if q_info.queue_id == queue_id:
                queue_info = q_info
                break

        if not queue_info:
            queue_info = QueueInfo(queue_id)
            self.queues_info.append(queue_info)

        queue_info.update(data)


class QueueInfo(object):
    def __init__(self, queue_id):
        self.queue_id = queue_id
        self.avg_bytes = 0
        self.sum_bytes = 0
        self.msg_count = 0

    def update(self, msg):
        self.msg_count += 1
        self.sum_bytes += len(msg)
        self.avg_bytes = self.sum_bytes / self.msg_count


def main():
    stdscr = curses.initscr()
    curses.noecho()
    curses.cbreak()

    try:
        qt = QueueThread()
        qt.start()

        queues_info = qt.get_info()
        ths = ['Queue Name', 'Avg bytes', 'Sum bytes', '#']

        while qt.stopped() == False:
            stdscr.clear()

            height, width = stdscr.getmaxyx()
            cell_w = width / len(ths)

            for i in range(0, len(ths)):
                stdscr.addstr(0, i*cell_w, ths[i])

            for i in range(0, len(queues_info)):
                q_info = queues_info[i]

                stdscr.addstr(i + 1, 0, q_info.queue_id)
                stdscr.addstr(i + 1, 1 * cell_w, str(q_info.avg_bytes))
                stdscr.addstr(i + 1, 2 * cell_w, str(q_info.sum_bytes))
                stdscr.addstr(i + 1, 3 * cell_w, str(q_info.msg_count))

            stdscr.refresh()

            time.sleep(1)
    except KeyboardInterrupt:
        pass
    finally:
        qt.stop()
        qt.join()

        curses.echo()
        curses.nocbreak()
        curses.endwin()

        if qt.error:
            print(qt.error)
        else:
            print('Exit ok')

        sys.exit(0)
