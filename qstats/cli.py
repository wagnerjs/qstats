import argparse
import config
import qstats

# ======================== CLI Commands & Arguments ===========================


def create_parser():
    parser = argparse.ArgumentParser(
        description='Tool to monitor queues'
    )
    parser.add_argument(
        '--rmq-host',
        default=config.RMQ_HOST,
        help='Default RabbitMQ host: {}'.format(config.RMQ_HOST),
        type=str,
    )
    parser.add_argument(
        '--rmq-port',
        default=config.RMQ_PORT,
        help='Default RabbitMQ port: {}'.format(config.RMQ_PORT),
        type=int,
    )
    parser.add_argument(
        '--rmq-vhost',
        default=config.RMQ_VHOST,
        help='Default RabbitMQ VHost: {}'.format(config.RMQ_VHOST),
        type=str,
    )
    parser.add_argument(
        '--rmq-user',
        default=config.RMQ_USER,
        help='Default RabbitMQ user: {}'.format(config.RMQ_USER),
        type=str,
    )
    parser.add_argument(
        '--rmq-pass',
        default=config.RMQ_PASSWORD,
        help='Default RabbitMQ password: {}'.format(config.RMQ_PASSWORD),
        type=str,
    )
    parser.add_argument(
        '--rmq-queues',
        default=config.RMQ_QUEUES,
        help='Default RabbitMQ queues: {}'.format(config.RMQ_QUEUES),
        nargs='*',
        type=str,
    )

    return parser


def main():
    """Tool to monitor queues."""

    args = create_parser().parse_args()

    config.RMQ_HOST = args.rmq_host
    config.RMQ_PORT = args.rmq_port
    config.RMQ_USER = args.rmq_user
    config.RMQ_PASSWORD = args.rmq_pass
    config.RMQ_VHOST = args.rmq_vhost
    config.RMQ_QUEUES = args.rmq_queues

    qstats.main()
